const gameContainer = document.getElementById("game");

// const COLORS = [
//   "red",
//   "blue",
//   "green",
//   "orange",
//   "purple",
//   "red",
//   "blue",
//   "green",
//   "orange",
//   "purple"
// ];

const easyGifs = [
  "./gifs/1.png",
  "./gifs/2.png",
  "./gifs/3.png",
  "./gifs/1.png",
  "./gifs/2.png",
  "./gifs/3.png"
]

const intermediateGifs = [
  "./gifs/1.png",
  "./gifs/2.png",
  "./gifs/3.png",
  "./gifs/4.png",
  "./gifs/1.png",
  "./gifs/2.png",
  "./gifs/3.png",
  "./gifs/4.png"
]
const difficultGifs = [
  "./gifs/1.png",
  "./gifs/2.png",
  "./gifs/3.png",
  "./gifs/4.png",
  "./gifs/5.png",
  "./gifs/6.png",
  "./gifs/1.png",
  "./gifs/2.png",
  "./gifs/3.png",
  "./gifs/4.png",
  "./gifs/5.png",
  "./gifs/6.png"
]

function Prop() {
  this.gifs = 0;
  this.matchCount = 0;
  this.guesses = 0;
  this.gameEnded = false;
}

const Props = new Prop();

// initial number of gifs
Props.gifs = 6;

// function for preloading of gifs 
function preloadGifs(gifs) {
  const preloadedGifs = [];

  for (let index = 0; index < gifs.length; index++) {
    preloadedGifs[index] = new Image();
    preloadedGifs[index].src = gifs[index];
  }

  return preloadedGifs;
}

// preloading of the cover gif
const cover = ["./gifs/8.png"]
const preloadedCover = [];

function preloadCover() {
  for (let index = 0; index < cover.length; index++) {
    preloadedCover[index] = new Image();
    preloadedCover[index].src = cover[index];
  }
}

preloadCover();

// initially sets 6 number of divs
const shuffledGifs = shuffle(preloadGifs(easyGifs));

createDivsForGifs(shuffledGifs);

// on difficulty change event, changes the number of divs
document.querySelector("#option").addEventListener("change", () => {
  if (document.querySelector("#option").value == "easy") {
    const shuffledGifs = shuffle(preloadGifs(easyGifs));

    createDivsForGifs(shuffledGifs);

    Props.gifs = 6;
  } else if (document.querySelector("#option").value == "intermediate") {
    const shuffledGifs = shuffle(preloadGifs(intermediateGifs));

    createDivsForGifs(shuffledGifs);

    Props.gifs = 8;
  } else if (document.querySelector("#option").value == "hard") {
    const shuffledGifs = shuffle(preloadGifs(difficultGifs));

    createDivsForGifs(shuffledGifs);

    Props.gifs = 12;
  }
})

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];

    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

// let shuffledColors = shuffle(COLORS);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
// function createDivsForColors(colorArray) {
//   for (let color of colorArray) {
//     // create a new div
//     const newDiv = document.createElement("div");

//     newDiv.style = "background: white;";
//     // give it a class attribute for the value we are looping over
//     newDiv.classList.add(color);

//     // call a function handleCardClick when a div is clicked on
//     newDiv.addEventListener("click", handleCardClick);

//     // append the div to the element with an id of game
//     gameContainer.append(newDiv);
//   }
// }

function createDivsForGifs(gifs) {
  gameContainer.innerHTML = " ";

  for (let gif of gifs) {
    // create a new div
    const newDiv = document.createElement("div");

    newDiv.style = "background-image: url(" + preloadedCover[0].src + "); background-size: cover; background-repeat: no-repeat;";
    // give it a class attribute for the value we are looping over
    newDiv.classList.add(gif.src);

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}

// variables for handleClick function
let colored = [];

// setting up the initial bestScore in localstorage
if (localStorage.getItem("bestScore") == undefined) {
  localStorage.setItem("bestScore", 0);
}

document.querySelector("#bestScore").innerText = localStorage.getItem("bestScore");

// TODO: Implement this function!
function handleCardClick(event) {
  // you can use event.target to see which element was clicked
  const target = event.target;

  if (target.classList[1] != "clicked" && colored.length < 2) {
    Props.guesses += 1;

    console.log(Props.guesses)
    document.querySelector("#guesses").innerText = Props.guesses;

    const gif = event.target.className;

    target.style = `background-image: url(${gif}); background-size: cover; transition-duration: 1000ms; transform: rotateY(180deg);`;
    target.classList.add("clicked");

    colored.push(target);

    if (colored.length == 2 && colored[0].className === colored[1].className) {
      setTimeout(() => {
        colored = [];

        Props.matchCount += 1;

        if (Props.matchCount == Props.gifs / 2) {
          Props.gameEnded = true;
          document.querySelector(".difficulty").style = "display: inline-block;"

          if (localStorage.getItem("bestScore") == 0) {
            localStorage.setItem("bestScore", Props.guesses);

            document.querySelector("#bestScore").innerText = localStorage.getItem("bestScore");
          }

          document.querySelector("#game").classList.add("hidden");
          document.querySelector("#score").classList.add("hidden");
          document.querySelector("#message").style = "display: inline-block;";

          if (Props.guesses >= localStorage.getItem("bestScore")) {
            document.querySelector("#message").innerText = `You have guessed all the cards. Score: ${Props.guesses}`;
          } else {
            document.querySelector("#message").innerText = `Congrats you've beaten the best score. Your score: ${Props.guesses}`;
          }

          document.querySelector("#btn").style = "display: inline-block;"

          if (localStorage.getItem("bestScore") > Props.guesses) {
            localStorage.setItem("bestScore", Props.guesses);

            document.querySelector("#bestScore").innerText = localStorage.getItem("bestScore");
          }
        }
      }, 1 * 1000);
    } else if (colored.length == 2) {
      setTimeout(() => {
        colored[0].style = "background-image: url(" + preloadedCover[0].src + "); background-size: cover; transition-duration: 1000ms; transform: rotateY(180deg); ";
        colored[1].style = "background-image: url(" + preloadedCover[0].src + "); background-size: cover; transition-duration: 1000ms; transform: rotateY(180deg);";

        colored[0].classList.remove("clicked");
        colored[1].classList.remove("clicked");

        colored = [];
      }, 1 * 1000);
    }
  }
}

// Start and Restart;
document.querySelector("#btn").addEventListener("click", (e) => {
  document.querySelector(".difficulty").style = "display: none;"
  document.querySelector("#game").classList.remove("hidden");
  document.querySelector("#score").classList.remove("hidden");
  document.querySelector("h1").classList.add("hidden");
  document.querySelector("#bestScoreDiv").style = "display: none";

  if (e.target.innerText == "Restart" && Props.gameEnded == true) {
    Props.gameEnded = false;
    
    document.querySelector("#game").classList.remove("hidden");
    document.querySelector("#score").classList.remove("hidden");
    document.querySelector("#message").style = "display: none;"

    document.querySelectorAll("#game > div").forEach(element => {
      element.classList.remove("clicked");
      element.style = "background-image: url(" + preloadedCover[0].src + "); background-size: cover; transition-duration: 1000ms; transform: rotateY(180deg);";

      Props.guesses = 0;

      document.querySelector("#guesses").innerText = Props.guesses;

      Props.matchCount = 0;
    })
  } else if (e.target.innerText == "Restart" && Props.gameEnded == false) {
    document.querySelector("#modal").style = "display: block;";

    document.querySelector("#yes").addEventListener("click", () => {
      document.querySelector("#modal").style = "display: none;";
      document.querySelector("#game").classList.remove("hidden");
      document.querySelector("#score").classList.remove("hidden");
      document.querySelector("#message").style = "display: none;"

      document.querySelectorAll("#game > div").forEach(element => {
        element.classList.remove("clicked");
        element.style = "background-image: url(" + preloadedCover[0].src + "); background-size: cover; transition-duration: 1000ms; transform: rotateY(180deg);";

        Props.guesses = 0;

        document.querySelector("#guesses").innerText = Props.guesses;

        Props.matchCount = 0;
      })
    })

    document.querySelector("#no").addEventListener("click", () => {
      document.querySelector("#modal").style = "display: none;";
    })
  }

  if (e.target.innerText != "Restart") {
    e.target.innerText = "Restart";
  }
})